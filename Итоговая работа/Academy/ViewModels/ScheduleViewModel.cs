﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Academy.Models;
using Academy.ViewModels.Commands;
using Microsoft.EntityFrameworkCore;

namespace Academy.ViewModels
{
    class ScheduleViewModel : INotifyPropertyChanged
    {
        private readonly AcademyContext _context = new();

        public event PropertyChangedEventHandler PropertyChanged;
        public CreateScheduleCommand CreateScheduleCommand { get; set; }
        public DeleteScheduleCommand DeleteScheduleCommand { get; set; }
        public Schedule NewSchedule { get; set; }
        public List<Subject> Subjects { get; set; }
        public List<Teacher> Teachers { get; set; }
        public List<Group> Groups { get; set; }
        public List<LectureHall> LectureHalls { get; set; }

        private ObservableCollection<Schedule> _schedules;
        public ObservableCollection<Schedule> Schedules {
            get => _schedules;
            set
            {
                _schedules = value;
                NotifyPropertyChanged();
            }
        }
        public ScheduleViewModel()
        {
            Subjects = _context.Subjects.ToList();
            Teachers = _context.Teachers.ToList();
            Groups = _context.Groups.ToList();
            LectureHalls = _context.LectureHalls.ToList();
            Schedules = new ObservableCollection<Schedule>(_context.Schedules.ToList());

            NewSchedule = new Schedule();
            NewSchedule.Subject = Subjects.FirstOrDefault();
            NewSchedule.Teacher = Teachers.First();
            NewSchedule.DateTime = DateTime.UtcNow;
            NewSchedule.Group = Groups.FirstOrDefault();


            CreateScheduleCommand = new CreateScheduleCommand(this);
            DeleteScheduleCommand = new DeleteScheduleCommand(this);
        }

        public Schedule SelectedSchedule { get; set; }

        public void CreateSchedule()
        {
            try
            {
                NewSchedule.SubjectFK = NewSchedule.Subject.Id;
                NewSchedule.TeacherFK = NewSchedule.Teacher.Id;
                NewSchedule.GroupFK = NewSchedule.Group.Id;
                NewSchedule.LectureHallFK = NewSchedule.LectureHalls.Id;

                _context.Schedules.Add(NewSchedule);
                _context.SaveChanges();

                Schedules.Add(NewSchedule);
                MessageBox.Show("New schedule added", "Message", MessageBoxButton.OK);
            }
            catch (DbUpdateException)
            {
                MessageBox.Show("Unable to add a schedule to a past date", "ERROR", MessageBoxButton.OK);
            }           
            catch (NullReferenceException)
            {
                MessageBox.Show("Fill in all the fields", "ERROR", MessageBoxButton.OK);
            }
        }

        public void DeleteSchedule()
        {
            try
            {
                _context.Schedules.Remove(SelectedSchedule);
                _context.SaveChanges();

                Schedules.Remove(SelectedSchedule);
                MessageBox.Show("Selected schedule removed", "Message", MessageBoxButton.OK);
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("Select a schedule ", "ERROR", MessageBoxButton.OK);
            }
            
        }

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
