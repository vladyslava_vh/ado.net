﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Academy.ViewModels.Commands
{
    class CreateScheduleCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        public ScheduleViewModel ScheduleViewModel { get; set; }
        public CreateScheduleCommand(ScheduleViewModel scheduleViewModel)
        {
            ScheduleViewModel = scheduleViewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            ScheduleViewModel.CreateSchedule();
        }
    }
}
