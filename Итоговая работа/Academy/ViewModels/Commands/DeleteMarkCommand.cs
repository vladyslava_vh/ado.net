﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Academy.ViewModels.Commands
{
    class DeleteMarkCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        public MarkViewModel ViewModel { get; set; }
        public DeleteMarkCommand(MarkViewModel viewModel)
        {
            ViewModel = viewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            ViewModel.DeleteMyMark();
        }
    }
}
