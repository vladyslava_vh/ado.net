﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Academy.ViewModels.Commands
{
    class EditStudentCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        public GroupsViewModel GroupsViewModel { get; set; }
        public EditStudentCommand(GroupsViewModel groupsViewModel)
        {
            GroupsViewModel = groupsViewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            GroupsViewModel.EditStudent();
        }
    }
}
