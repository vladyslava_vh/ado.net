﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime;
using Academy.Models;
using System.Runtime.CompilerServices;
using Microsoft.EntityFrameworkCore;
using System.Windows.Controls;
using System.Windows;
using System.Collections.ObjectModel;

namespace Academy.ViewModels
{
    class FacultiesViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        
        private readonly AcademyContext _context = new();

        private ObservableCollection<Faculties> _faculties;
        public ObservableCollection<Faculties> Faculties
        {
            get => _faculties;
            set
            {
                _faculties = value;
                NotifyPropertyChanged();
            }
        }

        private Faculties _selectedFaculty;
        public Faculties SelectedFaculty {
            get => _selectedFaculty;
            set
            {
                _selectedFaculty = value;
                DepartmentsOfFaculty = new ObservableCollection<Department>(
                    _context.Faculties
                    .Where(f => f.Name == _selectedFaculty.Name)
                    .Single().Departments);
            }
        }

        public FacultiesViewModel()
        {
            Faculties = new ObservableCollection<Faculties>(_context.Faculties.Include(f => f.Departments).ToList());
            SelectedFaculty = _context.Faculties.FirstOrDefault();
        }

        private ObservableCollection<Department> _departmentsOfFaculty;
        public ObservableCollection<Department> DepartmentsOfFaculty {
            get => _departmentsOfFaculty;
            set
            {
                _departmentsOfFaculty = value;
                NotifyPropertyChanged();
            }
        }
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
