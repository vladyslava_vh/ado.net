﻿using Academy.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Academy.ViewModels
{
    class DepartmentsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private readonly AcademyContext _context = new();

        private ObservableCollection<Department> _departments;
        public ObservableCollection<Department> Departments {
            get => _departments;
            set
            {
                _departments = value;
                NotifyPropertyChanged();
            }
        }

        private Department _selectedDepartment;
        public Department SelectedDepartment {
            get => _selectedDepartment;
            set
            {
                _selectedDepartment = value;
                Teachers = new ObservableCollection<Teacher>(_selectedDepartment.Teachers.ToList());
                NotifyPropertyChanged();
            }
        }

        public DepartmentsViewModel()
        {
            Departments = new ObservableCollection<Department>(
                _context.Departments
                .Include(d => d.Teachers)
                .Include(d => d.Specialities)
                .Include(d => d.Faculty)
                .ToList());

            SelectedDepartment = Departments.FirstOrDefault();
        }

        public ObservableCollection<Teacher> Teachers { get; set; }

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
