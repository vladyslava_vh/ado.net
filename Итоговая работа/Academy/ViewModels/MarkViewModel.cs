﻿using Academy.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Academy.ViewModels.Commands;
using System.Windows;
using System.Windows.Data;
using System.Globalization;
using System.Collections.ObjectModel;

namespace Academy.ViewModels
{
    class MarkViewModel : INotifyPropertyChanged
    {
        private readonly AcademyContext _context = new();

        public List<int> Marks { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public AddMarkCommand AddMarkCommand { get; set; }
        public DeleteMarkCommand DeleteMarkCommand { get; set; }

        public MarkViewModel()
        {
            Schedules = _context.Schedules
                .Include(s => s.Subject)
                .Include(s => s.Group)
                .Include(s => s.LectureHalls)
                .Include(s => s.Teacher)
                .ToList();

            SelectedSchedule = Schedules.FirstOrDefault();
            AddMarkCommand = new AddMarkCommand(this);
            DeleteMarkCommand = new DeleteMarkCommand(this);
            AllDbMarks = new ObservableCollection<Mark>(_context.Marks.Include(m => m.Student).ToList());

            #region Marks
                Marks = new List<int>();
                Marks.Add(1);
                Marks.Add(2);
                Marks.Add(3);
                Marks.Add(4);
                Marks.Add(5);
                Marks.Add(6);
                Marks.Add(7);
                Marks.Add(8);
                Marks.Add(9);
                Marks.Add(10);
                Marks.Add(11);
                Marks.Add(12);
            #endregion
            
        }

        private ObservableCollection<Mark> _allMarks;
        public ObservableCollection<Mark> AllDbMarks {
            get => _allMarks;
            set
            {
                _allMarks = value;
                NotifyPropertyChanged();
            }
        }
        public void AddMyMark()
        {
            try
            {
                Mark mark = new Mark();
                mark.Date = SelectedSchedule.DateTime.Date;
                mark.StudentFK = SelectedStudent.Id;
                mark.Student = SelectedStudent;
                mark.SubjectFK = SelectedSchedule.SubjectFK;
                mark.Subject = SelectedSchedule.Subject;
                mark.Value = Mark;

                _context.Marks.Add(mark);
                _context.SaveChanges();

                AllDbMarks.Add(mark);
                MessageBox.Show("New mark added", "Message", MessageBoxButton.OK);
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Enter a student", "ERROR", MessageBoxButton.OK);
            }
            
        }

        private Mark _selectedDataGridMark;
        public Mark SelectedDataGridMark 
        {
            get => _selectedDataGridMark;
            set
            {
                _selectedDataGridMark = value;
                NotifyPropertyChanged();
            }
        }

        public void DeleteMyMark()
        {
            try
            {
                _context.Marks.Remove(SelectedDataGridMark);
                _context.SaveChanges();

                AllDbMarks.Remove(SelectedDataGridMark);
                MessageBox.Show("Mark deleted", "Message", MessageBoxButton.OK);
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("Mark list is empty or select a mark", "ERROR", MessageBoxButton.OK);
            }            
        }

        public List<Schedule> Schedules { get; set; }

        private Schedule _selectedSchedule;
        public Schedule SelectedSchedule {
            get => _selectedSchedule;
            set
            {
                _selectedSchedule = value;
                Students = _context.Students.Where(s => s.Group.Name == _selectedSchedule.Group.Name).ToList();
                NotifyPropertyChanged();
            }
        }

        private List<Student> _students;
        public List<Student> Students {
            get => _students;
            set
            {
                _students = value;
                NotifyPropertyChanged();
            }
        }

        private int _mark;
        public int Mark {
            get => _mark;
            set
            {
                _mark = value;
                NotifyPropertyChanged();
            }
        }

        private Student _selectedStudent;
        public Student SelectedStudent {
            get => _selectedStudent;
            set
            {
                _selectedStudent = value;
                NotifyPropertyChanged();
            }
        }
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
