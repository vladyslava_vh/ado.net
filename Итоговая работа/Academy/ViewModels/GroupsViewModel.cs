﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime;
using Academy.Models;
using System.Runtime.CompilerServices;
using Microsoft.EntityFrameworkCore;
using System.Windows.Controls;
using System.Windows.Input;
using Academy.ViewModels.Commands;
using System.Windows;
using System.Collections.ObjectModel;

namespace Academy.ViewModels
{
    class GroupsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public EditStudentCommand EditStudentCommand { get; set; }
        public DeleteStudentCommand DeleteStudentCommand { get; set; }
        public DeleteGroupCommand DeleteGroupCommand { get; set; }
        public EditGroupCommand EditGroupCommand { get; set; }

        private readonly AcademyContext _context = new();
        public GroupsViewModel()
        {
            Students = new ObservableCollection<Student>(_context.Students.ToList());
            Groups = new ObservableCollection<Group>(_context.Groups.Include(g => g.Speciality).ToList());
            Specialities = _context.Specialities.Select(s => s.Name).ToList();

            SelectedGroup = _groups.Select(g => g.Name).FirstOrDefault();

            EditStudentCommand = new EditStudentCommand(this);
            DeleteStudentCommand = new DeleteStudentCommand(this);
            DeleteGroupCommand = new DeleteGroupCommand(this);
            EditGroupCommand = new EditGroupCommand(this);
        }

        public List<string> Specialities { get; set; }

        private ObservableCollection<Student> _students;
        public ObservableCollection<Student> Students {
            get => _students;
            set
            {
                _students = value;
                NotifyPropertyChanged();
            }
        }

        private Student _selectedStudent;
        public Student SelectedStudent {
            get => _selectedStudent;
            set
            {
                _selectedStudent = value;
                NotifyPropertyChanged();
            }
        }

        private ObservableCollection<Group> _groups;
        public ObservableCollection<Group> Groups
        {
            get => _groups;
            set
            {
                _groups = value;
                NotifyPropertyChanged();
            }
        }

        public List<string> GroupNames
        {
            get => _context.Groups.Select(g => g.Name).ToList();
        }

        private string _selectedGroup;
        public string SelectedGroup {
            get => _selectedGroup;
            set
            {
                _selectedGroup = value;
                Students = new ObservableCollection<Student>(_context.Students.Where(s => s.Group.Name == _selectedGroup).ToList());
                NotifyPropertyChanged();
            }
        }

        private Group _selectedDataGridGroup;
        public Group SelectedDataGridGroup {
            get => _selectedDataGridGroup; 
            set
            {
                _selectedDataGridGroup = value;
                NotifyPropertyChanged();
            }
        }

        public void EditStudent()
        {
            try
            {
                Student student = _context.Students.Find(SelectedStudent.Id);
                student.FirstName = SelectedStudent.FirstName;
                student.LastName = SelectedStudent.LastName;
                student.StudentStatus = SelectedStudent.StudentStatus;
                student.Phone = SelectedStudent.Phone;
                student.BirthDate = SelectedStudent.BirthDate;

                _context.SaveChanges();
                MessageBox.Show("All changes saved", "Message", MessageBoxButton.OK);
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Select a student", "ERROR", MessageBoxButton.OK);
            }
        }

        public void EditGroup()
        {
            try
            {
                Group group = _context.Groups.Find(SelectedDataGridGroup.Id);
                group.Name = SelectedDataGridGroup.Name;
                Specialities speciality = _context.Specialities.Where(s => s.Name == SelectedDataGridGroup.Speciality.Name).Single();
                group.Speciality = speciality;
                group.SpecialityFK = speciality.Id;
                group.Year = SelectedDataGridGroup.Year;

                _context.SaveChanges();
                MessageBox.Show("All changes saved", "Message", MessageBoxButton.OK);
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Select a group", "ERROR", MessageBoxButton.OK);
            }
        }

        public void DeleteStudent()
        {
            try
            {
                Student student = _context.Students.Find(SelectedStudent.Id);
                _context.Students.Remove(student);
                _context.SaveChanges();

                Students.Remove(SelectedStudent);
                MessageBox.Show("Student removed", "Message", MessageBoxButton.OK);
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Select a student", "ERROR", MessageBoxButton.OK);
            }
        }

        public void DeleteGroup()
        {
            try
            {
                Group group = _context.Groups.Find(SelectedDataGridGroup.Id);
                _context.Groups.Remove(group);
                _context.SaveChanges();

                Groups.Remove(SelectedDataGridGroup);
                MessageBox.Show("Group removed", "Message", MessageBoxButton.OK);
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Select a group", "ERROR", MessageBoxButton.OK);
            }
        }

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
