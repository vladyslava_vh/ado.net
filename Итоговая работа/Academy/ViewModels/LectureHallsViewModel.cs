﻿using Academy.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Academy.ViewModels
{
    class LectureHallsViewModel
    {
        private readonly AcademyContext _context = new();
        public LectureHallsViewModel()
        {
            LectureHalls = _context.LectureHalls.ToList();
            Subjects = _context.Subjects.ToList();
        }
        public List<LectureHall> LectureHalls { get; set; }
        public List<Subject> Subjects { get; set; }
    }
}
