﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.Models
{
    class Faculties
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Dean { get; set; }

        [Required]
        public int Phone { get; set; }

        [Required]
        public virtual ICollection<Department> Departments { get; set; }
    }
}
