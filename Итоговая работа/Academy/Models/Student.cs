﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.Models
{
    class Student
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        public int Phone { get; set; }

        [DataType("date")]
        public DateTime BirthDate { get; set; }

        [Required]
        [StringLength(25)]
        public string StudentStatus { get; set; }

        [Column("GroupFK")]
        public int GroupFK { get; set; }

        [ForeignKey("GroupFK")]
        public virtual Group Group { get; set; }

        [Required]
        public virtual ICollection<Mark> Marks { get; set; }
    }
}
