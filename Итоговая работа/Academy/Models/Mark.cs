﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.Models
{
    class Mark
    {
        [Required]
        [Column("StudentFK")]
        public int StudentFK { get; set; }

        [ForeignKey("StudentFK")]
        public virtual Student Student { get; set; }

        [Required]
        [Column("SubjectFK")]
        public int SubjectFK { get; set; }

        [ForeignKey("SubjectFK")]
        public virtual Subject Subject { get; set; }

        public int Value { get; set; }

        [DataType("datetime2")]
        public DateTime Date { get; set; }
    }
}
