﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Academy.Models
{
    class AcademyContext : DbContext
    {
        public DbSet<Faculties> Faculties { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Specialities> Specialities { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<LectureHall> LectureHalls { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Mark> Marks { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connectionString = "Server=(local);Database=Academy;Trusted_Connection=True;MultipleActiveResultSets=true";

            optionsBuilder.UseSqlServer(connectionString);
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.LogTo(s => System.Diagnostics.Debug.WriteLine(s));
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Mark>().HasKey(m => new { m.SubjectFK, m.StudentFK });
        }
    }
}
