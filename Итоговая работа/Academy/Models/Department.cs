﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.Models
{
    class Department
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string DepartmentHead { get; set; }

        [Column("FacultyFK")]
        public int FacultyFK { get; set; }

        [ForeignKey("FacultyFK")]
        public virtual Faculties Faculty { get; set; }

        [Required]
        public int Phone { get; set; }

        [Required]
        public virtual ICollection<Specialities> Specialities { get; set; }

        [Required]
        public virtual ICollection<Teacher> Teachers { get; set; }
    }
}
