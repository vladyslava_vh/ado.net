﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.Models
{
    class Specialities
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Column("DepartmentFK")]
        public int DepartmentFK { get; set; }

        [ForeignKey("DepartmentFK")]
        public virtual Department Department { get; set; }// Навигационное свойство

        [Required]
        public virtual ICollection<Group> Groups { get; set; }
    }
}
