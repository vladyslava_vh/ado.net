﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.Models
{
    class Group
    {
        public int Id { get; set; }

        [Required]
        [StringLength(10)]
        public string Name { get; set; }

        [Column("SpecialityFK")]
        public int SpecialityFK { get; set; }

        [ForeignKey("SpecialityFK")]
        public Specialities Speciality { get; set; }

        [Required]
        public int Year { get; set; }

        [Required]
        public virtual ICollection<Schedule> Schedules { get; set; }

        [Required]
        public virtual ICollection<Student> Students { get; set; }
    }
}
