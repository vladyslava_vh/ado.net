﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.Models
{
    class Schedule
    {
        public int Id { get; set; }

        [Column("SubjectFK")]
        public int SubjectFK { get; set; }

        [ForeignKey("SubjectFK")]
        public virtual Subject Subject { get; set; }

        [DataType("datetime")]
        public DateTime DateTime { get; set; }

        [Column("TeacherFK")]
        public int TeacherFK { get; set; }

        [ForeignKey("TeacherFK")]
        public virtual Teacher Teacher { get; set; }

        [Column("GroupFK")]
        public int GroupFK { get; set; }

        [ForeignKey("GroupFK")]
        public virtual Group Group { get; set; }

        [Column("LectureHallFK")]
        public int LectureHallFK { get; set; }

        [ForeignKey("LectureHallFK")]
        public virtual LectureHall LectureHalls{ get; set; }
    }
}
