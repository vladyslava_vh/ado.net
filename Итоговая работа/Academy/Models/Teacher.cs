﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Academy.Models
{
    class Teacher : INotifyPropertyChanged
    {
        public int Id { get; set; }

        [Required]
        [StringLength(30)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(30)]
        public string LastName { get; set; }

        [Column("DepartmentFK")]
        public int DepartmentFK { get; set; }

        [ForeignKey("DepartmentFK")]
        public virtual Department Department { get; set; }// Навигационное свойство

        [Required]
        public int Phone { get; set; }

        [Required]
        public virtual ICollection<Schedule> Schedules { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}