﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Academy.Windows
{
    /// <summary>
    /// Interaction logic for UniversityInfo.xaml
    /// </summary>
    public partial class UniversityInfo : Window
    {
        private readonly Faculties _facultiesWnd;

        private readonly DepartmentsView _departmentsWnd;

        private readonly LectureHallsView _lectureHallsWnd;
        public UniversityInfo()
        {
            InitializeComponent();
            Closing += UniversityInfo_Closing;
            _facultiesWnd = new Faculties();
            _departmentsWnd = new DepartmentsView();
            _lectureHallsWnd = new LectureHallsView();
        }

        private void UniversityInfo_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Visibility = Visibility.Hidden;
        }

        private void Faculties_Click(object sender, RoutedEventArgs e)
        {
            if (_facultiesWnd.ShowDialog() == true)
            {
                _facultiesWnd.Visibility = Visibility.Visible;
            }
        }

        private void Departments_Click(object sender, RoutedEventArgs e)
        {
            if (_departmentsWnd.ShowDialog() == true)
            {
                _departmentsWnd.Visibility = Visibility.Visible;
            }
        }

        private void LectureHalls_Click(object sender, RoutedEventArgs e)
        {
            if (_lectureHallsWnd.ShowDialog() == true)
            {
                _lectureHallsWnd.Visibility = Visibility.Visible;
            }
        }
    }
}
