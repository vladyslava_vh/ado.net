﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Academy.Windows
{
    /// <summary>
    /// Interaction logic for LectureHallsView.xaml
    /// </summary>
    public partial class LectureHallsView : Window
    {
        public LectureHallsView()
        {
            InitializeComponent();
            Closing += LectureHallsView_Closing;
        }

        private void LectureHallsView_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Visibility = Visibility.Hidden;
        }
    }
}
