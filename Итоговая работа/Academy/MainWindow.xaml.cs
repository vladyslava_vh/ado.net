﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Academy.Windows;

namespace Academy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly UniversityInfo _universityInfo;
        private readonly StudentInfo _studentInfoWnd;
        private readonly AddScheduleView _addScheduleWnd;
        private readonly AddMarkView _addMarkWnd;
        public MainWindow()
        {
            InitializeComponent();
            _studentInfoWnd = new StudentInfo();
            _universityInfo = new UniversityInfo();
            _addScheduleWnd = new AddScheduleView();
            _addMarkWnd = new AddMarkView();
            Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Close();
        }

        private void UniversityInfo_Click(object sender, RoutedEventArgs e)
        {
            if (_universityInfo.ShowDialog() == true)
            {
                _universityInfo.Visibility = Visibility.Visible;
            }
        }

        private void GroupsInfo_Click(object sender, RoutedEventArgs e)
        {
            if (_studentInfoWnd.ShowDialog() == true)
            {
                _studentInfoWnd.Visibility = Visibility.Visible;
            }
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void AddSchedule_Click(object sender, RoutedEventArgs e)
        {
            if (_addScheduleWnd.ShowDialog() == true)
            {
                _addScheduleWnd.Visibility = Visibility.Visible;
            }
        }

        private void AddMark_Click(object sender, RoutedEventArgs e)
        {
            if (_addMarkWnd.ShowDialog() == true)
            {
                _addMarkWnd.Visibility = Visibility.Visible;
            }
        }
    }
}
