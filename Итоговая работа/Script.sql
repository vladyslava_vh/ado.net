IF DB_ID('Academy') IS NOT NULL
BEGIN
	USE master;
	ALTER DATABASE Academy SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
    DROP DATABASE Academy;
END
GO

CREATE DATABASE Academy;
GO

USE Academy
GO

CREATE TABLE Faculties
(
	Id     int           NOT NULL IDENTITY PRIMARY KEY,
	[Name] nvarchar(100) NOT NULL UNIQUE CHECK ([Name] <> N''),
	Dean   nvarchar(50)  NOT NULL UNIQUE CHECK (Dean <> N''),
	Phone  int           NOT NULL UNIQUE
);
GO

CREATE TABLE Departments
(
	Id		       int           NOT NULL IDENTITY PRIMARY KEY,
	[Name]	       nvarchar(100) NOT NULL UNIQUE CHECK ([Name] <> N''),
	DepartmentHead nvarchar(50)  NOT NULL UNIQUE CHECK (DepartmentHead <> N''),
	FacultyFK      int           NOT NULL FOREIGN KEY REFERENCES Faculties(Id) ON DELETE NO ACTION ON UPDATE CASCADE,
	Phone          int			 NOT NULL UNIQUE
);
GO

CREATE TABLE Teachers
(
	Id           int          NOT NULL IDENTITY PRIMARY KEY,
	FirstName    nvarchar(30) NOT NULL CHECK (FirstName <> N''),
	LastName     nvarchar(30) NOT NULL CHECK (LastName <> N''),
	DepartmentFK int          NOT NULL FOREIGN KEY REFERENCES Departments(Id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	Phone        int		  NOT NULL UNIQUE
);
GO

CREATE TABLE Specialities
(
	Id		     int           NOT NULL IDENTITY PRIMARY KEY,
	[Name]	     nvarchar(100) NOT NULL UNIQUE CHECK ([Name] <> N''),
	DepartmentFK int           NOT NULL FOREIGN KEY REFERENCES Departments(Id) ON DELETE NO ACTION ON UPDATE CASCADE
);
GO

CREATE TABLE Groups
(
	Id        int          NOT NULL IDENTITY PRIMARY KEY,
	[Name]    nvarchar(10) NOT NULL UNIQUE CHECK ([Name] <> N''),
	SpecialityFK int       NOT NULL FOREIGN KEY REFERENCES Specialities(Id) ON DELETE NO ACTION ON UPDATE CASCADE,
	[Year]    int          NOT NULL CHECK ([Year] >= 2018)
);
GO

CREATE TABLE Students
(
	Id			  int IDENTITY(1, 1)  NOT NULL  PRIMARY KEY,
	FirstName	  nvarchar(50)        NOT NULL,
	LastName	  nvarchar(50)        NOT NULL,
	Phone         int				  NOT NULL  UNIQUE,
	BirthDate	  Date			      NULL      CHECK(DATEDIFF(YEAR, BirthDate, SYSDATETIME()) >= 16 ),
	StudentStatus nvarchar(25)	      NOT NULL, 
	GroupFK       int                 NOT NULL  FOREIGN KEY REFERENCES Groups(Id) ON DELETE NO ACTION ON UPDATE CASCADE 
)
GO

CREATE TABLE Subjects
(
	Id       int IDENTITY NOT NULL PRIMARY KEY,
	[Name]   nvarchar(50) NOT NULL UNIQUE, 
	Duration int      NOT NULL
)
GO

CREATE TABLE LectureHalls
(
	Id       int IDENTITY NOT NULL PRIMARY KEY,
	Number   int      NOT NULL UNIQUE
)
GO

CREATE TABLE Schedules
(
	Id             int IDENTITY NOT NULL PRIMARY KEY,
	SubjectFK	   int      NOT NULL REFERENCES Subjects(Id) ON DELETE NO ACTION ON UPDATE CASCADE,
	[DateTime]	   datetime NOT NULL CHECK([DateTime] >= SYSDATETIME()),
	TeacherFK	   int      NOT NULL REFERENCES Teachers(Id) ON DELETE NO ACTION ON UPDATE CASCADE,
	GroupFK		   int      NOT NULL REFERENCES Groups(Id)   ON DELETE NO ACTION ON UPDATE NO ACTION,
	LectureHallFK  int      NOT NULL REFERENCES LectureHalls(Id) ON DELETE NO ACTION ON UPDATE CASCADE
)
GO


CREATE TABLE Marks
(
	StudentFK int       NOT NULL REFERENCES Students(Id) ON DELETE NO ACTION ON UPDATE CASCADE,
	SubjectFK int       NOT NULL REFERENCES Subjects(Id) ON DELETE NO ACTION ON UPDATE CASCADE,
	[Value]   int       NOT NULL CHECK(Value BETWEEN 1 AND 12),
	[Date]    DateTime2 NOT NULL DEFAULT(SYSDATETIME()),
	PRIMARY KEY (StudentFK, SubjectFK)
)

--заполнение
USE Academy
GO

INSERT INTO Faculties([Name], Dean, Phone)
VALUES ('Computer sciences', 'Ivan Ivanov', 0961212121)

INSERT INTO Departments([Name], DepartmentHead, [Phone], FacultyFK)
VALUES ('Programming', 'Vasya Pupkin', 0987321354, 1),
	   ('Desing', 'Maria Vilkina', 0679995552, 1),
	   ('Administration', 'Max Dividenko', 0987999454, 1)

INSERT INTO Specialities([Name], DepartmentFK)
VALUES ('Software development', 1), 
	   ('Computer graphics and design', 2), 
	   ('Computer networks and cybersecurity', 3)

INSERT INTO Subjects([Name], Duration)
VALUES ('Procedural programming in C ++', 48), ('OOP in C++', 55),
	   ('C#', 30), ('HTML/CSS', 20), ('Photoshop', 27), ('UI/UX', 15),
	   ('Information security basics', 12)

INSERT INTO Groups([Name], SpecialityFK, [Year])
VALUES ('CS-181', 1, 2018), ('CS-182', 2, 2018), ('CS-191', 2, 2019), ('CS-192', 1, 2019),
	   ('CS-193', 3, 2019), ('CS-201', 3, 2020), ('CS-202', 1, 2020), ('CS-203', 2, 2020)

INSERT INTO Students(FirstName, LastName, StudentStatus, GroupFK, Phone, BirthDate)
VALUES ('Makar', 'Rodionov', 'actual', 1, 0965674749, '2001-11-05'),
	   ('Borislav', 'Isakov', 'for expulsion', 1, 0675674973, '2000-06-14'),
	   ('Alexandra', 'Muravyova', 'for expulsion', 1, 0985607689, '2001-03-10'),
	   ('Vyacheslav', 'Vladimirov', 'actual', 2, 0555676361, '2001-05-15'),
	   ('Rada', 'Nazarova', 'actual', 2, 0995874325, '2000-08-04'),
	   ('Alena', 'Denisova', 'actual', 2, 0655679301, '2001-02-03'),
	   ('Olga', 'Suvorova', 'correspondence student', 3, 0967489036, '2002-06-14'),
	   ('Vitaly', 'Fedorov', 'actual', 3, 0685674321, '2001-04-07'),
	   ('Victoria', 'Ilyina', 'for expulsion', 3, 0965675681, '2002-06-17'),
	   ('Valery', 'Gulyaev', 'for expulsion', 4, 0985673502, '2002-09-25'),
	   ('Veniamin', 'Alexandrov', 'actual', 4, 0665663800, '2002-12-09'),
	   ('Vera', 'Noskova', 'correspondence student', 4, 0555000931, '2001-08-12'),
	   ('Ruslan', 'Sergeev', 'actual', 5, 0995674311, '2002-12-31'),
	   ('Arseny', 'Zimin', 'actual', 5, 0985646080, '2002-10-14'),
	   ('Khariton', 'Gulyaev', 'actual', 5, 0965644321, '2002-07-03'),
	   ('Varvara', 'Karpova', 'for expulsion', 6, 0685674333, '2003-05-15'),
	   ('Alla', 'Sharapova', 'correspondence student', 6, 0965674098, '2003-03-17'),
	   ('Stepan', 'Trofimov', 'for expulsion', 6, 0975654890, '2003-06-09'),
	   ('Galena', 'Dyachkova', 'actual', 7, 0965674321, '2003-02-02'),
	   ('Zhdan', 'Terentyev', 'actual', 7, 0995677530, '2003-10-22'),
	   ('Ksenia', 'Симонова', 'for expulsion', 7, 0555368901, '2002-11-27'),
	   ('Yakov', 'Smirnov', 'actual', 8, 0985647806, '2003-03-20'),
	   ('Elvira', 'Semyonova', 'actual', 8, 0975674738, '2003-02-18'),
	   ('Nazar', 'Platunov', 'actual', 8, 0665673310, '2002-09-21')

INSERT INTO Teachers(FirstName, LastName, DepartmentFK, Phone)
VALUES ('Svyatoslav', 'Tretyakov', 1, 0976543217),
	   ('Vsevolod', 'Kotov', 1, 0987093345),
	   ('Renata', 'Terentyeva', 1, 0665285295),
	   ('Alena', 'Yakovleva', 2, 0553275297),
	   ('Sergey', 'Biryukov', 2, 0673467093),
	   ('Kirill', 'Kazakov', 2, 0985432974),
	   ('Jan', 'Nesterov', 3, 0995438710),
	   ('Leonid', 'Matveev', 3, 0650934761)

INSERT INTO LectureHalls(Number)
VALUES (8), (6), (13), (21), (49), (74), (10), (25)