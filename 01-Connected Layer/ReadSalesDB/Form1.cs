﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ReadSalesDB
{
    public partial class Form : System.Windows.Forms.Form
    {
        private readonly string _connectionString; 
        private readonly string _defaultConnectionString;
        private readonly ConnectionStringSettings _settings;
        private readonly SqlConnection _connection;
        private string _query = default;
        private SqlCommand _command = default;
        private readonly StringBuilder _builder = new StringBuilder();
        public Form()
        {
            InitializeComponent();
            _defaultConnectionString = @"Data Source=(local);Initial Catalog=Sales;Integrated Security=True";
            _settings = ConfigurationManager.ConnectionStrings["Sales"];

            _connectionString = _settings != null ? _settings.ConnectionString : _defaultConnectionString;
            _connection = new SqlConnection(_connectionString);

            comboBox.Items.Add("Buyers");
            comboBox.Items.Add("Sellers");
            comboBox.Items.Add("Sales");
            comboBox.Text = "Select the database";
            comboBox.Enabled = true;
            comboBox.SelectedValueChanged += ComboBox_SelectedValueChanged;
            comboBox.KeyPress += ComboBox_KeyPress;

            FormClosing += Form_FormClosing;
        }
        private void Form_Load(object sender, EventArgs e)
        {
            try
            {
                _connection.Open();
            }
            catch (DbException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка при работе с БД", MessageBoxButtons.OK);
            }

        }
        private void ComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                listBox.Items.Clear();
                _builder.Clear();
                _query = $"SELECT * FROM {comboBox.SelectedItem}";
                _command = new SqlCommand(_query, _connection);

                using (SqlDataReader reader = _command.ExecuteReader())
                {
                    //названия колонок
                    var columns = Enumerable.Range(0, reader.FieldCount)
                        .Select(reader.GetName)
                        .ToList();

                    foreach (var column in columns)
                    {
                        _builder.AppendFormat($"{column}\t");
                    }

                    listBox.Items.Add(_builder.ToString());//верхняя строка с названиями колонок

                    //остальные строки
                    while (reader.Read())
                    {
                        listBox.Items.Add(ReadSingleRow(reader));
                    }
                }

                listBox.Focus();
            }
            catch (DbException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка при работе с БД", MessageBoxButtons.OK);
            }
        }
        private void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                _connection.Dispose();
            }
            catch (DbException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка при работе с БД", MessageBoxButtons.OK);
            }
        }
        private string ReadSingleRow(IDataRecord row)
        {
            _builder.Clear();

            for (int i = 0; i < row.FieldCount; i++)
            {
                _builder.AppendFormat($"{row[i]}\t");
            }

            return _builder.ToString();
        }
        private void ComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = (char)Keys.None;
        }

    }
}
