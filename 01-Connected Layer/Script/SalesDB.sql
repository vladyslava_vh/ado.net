IF DB_ID('Sales') IS NOT NULL
BEGIN
	USE master
    ALTER DATABASE Sales SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
    DROP DATABASE Sales;
END
GO

CREATE DATABASE Sales;
GO

USE Sales
GO

CREATE TABLE Buyers
(
	Id bigint IDENTITY NOT NULL PRIMARY KEY,
	FirstName nvarchar(15) NOT NULL,
	LastName nvarchar(30) NOT NULL
);
GO

CREATE TABLE Sellers
(
	Id bigint IDENTITY NOT NULL PRIMARY KEY,
	FirstName nvarchar(15) NOT NULL,
	LastName nvarchar(30) NOT NULL
);
GO

CREATE TABLE Sales
(
	Id bigint IDENTITY NOT NULL PRIMARY KEY,
	Id_Seller bigint NOT NULL,
	Id_Buyer bigint NOT NULL,
	Sale_Sum money NOT NULL,
	Sale_Date date NOT NULL,

	FOREIGN KEY (Id_Seller) REFERENCES Sellers(Id)
		ON UPDATE CASCADE
		ON DELETE NO ACTION,
	FOREIGN KEY (Id_Buyer) REFERENCES Buyers(Id)
		ON UPDATE CASCADE
		ON DELETE NO ACTION
);
GO

-- ��������� �������

SET IDENTITY_INSERT Buyers ON
INSERT INTO Buyers (Id, FirstName, LastName) VALUES (1, 'John', 'Doe');
INSERT INTO Buyers (Id, FirstName, LastName) VALUES (2, 'Bill', 'Freeman');
INSERT INTO Buyers (Id, FirstName, LastName) VALUES (3, 'Uno', 'Dirck');
INSERT INTO Buyers (Id, FirstName, LastName) VALUES (4, 'Erick', 'Rapid');
INSERT INTO Buyers (Id, FirstName, LastName) VALUES (5, 'Chris', 'Rea');
SET IDENTITY_INSERT Buyers OFF
GO

SET IDENTITY_INSERT Sellers ON
INSERT INTO Sellers (Id, FirstName, LastName) VALUES (1, 'Tommy', 'Lee');
INSERT INTO Sellers (Id, FirstName, LastName) VALUES (2, 'Jeck', 'Ward');
INSERT INTO Sellers (Id, FirstName, LastName) VALUES (3, 'Rick', 'Depper');
INSERT INTO Sellers (Id, FirstName, LastName) VALUES (4, 'Neo', 'Matrix');
INSERT INTO Sellers (Id, FirstName, LastName) VALUES (5, 'Trinity', 'Matrix');
SET IDENTITY_INSERT Sellers OFF
GO

SET IDENTITY_INSERT Sales ON
INSERT INTO Sales (Id, Id_Seller, Id_Buyer, Sale_Sum, Sale_Date) VALUES (1, 5, 1, 1200, '2021-01-16');
INSERT INTO Sales (Id, Id_Seller, Id_Buyer, Sale_Sum, Sale_Date) VALUES (2, 3, 1, 1900, '2020-11-27');
INSERT INTO Sales (Id, Id_Seller, Id_Buyer, Sale_Sum, Sale_Date) VALUES (3, 4, 2, 1150, '2020-09-11');
INSERT INTO Sales (Id, Id_Seller, Id_Buyer, Sale_Sum, Sale_Date) VALUES (4, 2, 2, 2700, '2021-02-25');
INSERT INTO Sales (Id, Id_Seller, Id_Buyer, Sale_Sum, Sale_Date) VALUES (5, 1, 3, 3000, '2021-03-01');
INSERT INTO Sales (Id, Id_Seller, Id_Buyer, Sale_Sum, Sale_Date) VALUES (6, 3, 3, 2500, '2020-05-01');
INSERT INTO Sales (Id, Id_Seller, Id_Buyer, Sale_Sum, Sale_Date) VALUES (7, 5, 4, 1000, '2021-03-08');
INSERT INTO Sales (Id, Id_Seller, Id_Buyer, Sale_Sum, Sale_Date) VALUES (8, 2, 5, 1300, '2021-03-31');
SET IDENTITY_INSERT Sales OFF
GO

-- ��������

SELECT *
FROM Buyers;
GO

SELECT *
FROM Sellers;
GO

SELECT *
FROM Sales;
GO

SELECT Buyers.FirstName, Buyers.LastName, Sale_Sum AS 'Sum', Sale_Date AS 'Date'
FROM Sales JOIN Sellers ON Sellers.Id = Id_Seller
		   JOIN Buyers ON Buyers.Id = Id_Buyer
WHERE Sellers.FirstName = 'Trinity' AND Sellers.LastName = 'Matrix'