﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace LINQToXML
{
    public partial class Form : System.Windows.Forms.Form
    {
        private XDocument _xdoc;
        private XElement _root;
        public Form()
        {
            InitializeComponent();

            _xdoc = XDocument.Load("Gems.xml");
            _root = _xdoc.Root;
            dataGrid.RowHeadersVisible = false;
            comboBox.KeyPress += ComboBox_KeyPress;
            dataGrid.Focus();
        }

        private void ComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = (char)Keys.None;
        }

        private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGrid.Rows.Clear();
            dataGrid.Refresh();

            var gems = _root.Descendants("gem")
                .Where(g => g.Attribute("color").Value == comboBox.Text)
                .Select(g => new
                {
                    Name = g.Element("name").Value,
                    Type = g.Element("type").Value,
                    Transparency = bool.Parse(g.Attribute("transparency").Value)
                });

            foreach (var g in gems)
            {
                dataGrid.Rows.Add(g.Name, g.Type, g.Transparency);
            }

            dataGrid.Focus();
        }
    }
}
