IF DB_ID('WorkDays') IS NOT NULL
BEGIN
	USE master
    ALTER DATABASE WorkDays SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
    DROP DATABASE WorkDays;
END
GO

CREATE DATABASE WorkDays;
GO

USE WorkDays
GO

CREATE TABLE Schedules 
(
	Id bigint IDENTITY NOT NULL PRIMARY KEY,
	FirstName nvarchar(15) NOT NULL,
	LastName nvarchar(30) NOT NULL,
	DateIn date NOT NULL,
	DateOut date NOT NULL
);
GO

CREATE TABLE WorkDays
(
	Id bigint IDENTITY NOT NULL PRIMARY KEY,
	WeekdayNumber int NOT NULL,
	Id_Schedule bigint NOT NULL,

	FOREIGN KEY (Id_Schedule) REFERENCES Schedules(Id)
		ON UPDATE CASCADE
		ON DELETE NO ACTION
);
GO

-- ��������� �������

SET IDENTITY_INSERT Schedules ON
INSERT INTO Schedules (Id, FirstName, LastName, DateIn, DateOut) VALUES (1, 'John', 'Doe', '2009-01-01', '2009-04-30');
INSERT INTO Schedules (Id, FirstName, LastName, DateIn, DateOut) VALUES (2, 'Bill', 'Freeman', '2015-09-17', '2017-06-09');
INSERT INTO Schedules (Id, FirstName, LastName, DateIn, DateOut) VALUES (3, 'Uno', 'Dirck', '2018-11-30', '2020-02-03');
INSERT INTO Schedules (Id, FirstName, LastName, DateIn, DateOut) VALUES (4, 'Erick', 'Rapid', '2012-08-25', '2014-04-19');
INSERT INTO Schedules (Id, FirstName, LastName, DateIn, DateOut) VALUES (5, 'Chris', 'Rea', '2019-01-08', '2021-02-18');
SET IDENTITY_INSERT Schedules OFF
GO

SET IDENTITY_INSERT WorkDays ON
INSERT INTO WorkDays (Id, WeekdayNumber, Id_Schedule) VALUES (1, 1, 1);
INSERT INTO WorkDays (Id, WeekdayNumber, Id_Schedule) VALUES (2, 3, 1);
INSERT INTO WorkDays (Id, WeekdayNumber, Id_Schedule) VALUES (3, 5, 1);
INSERT INTO WorkDays (Id, WeekdayNumber, Id_Schedule) VALUES (4, 2, 2);
INSERT INTO WorkDays (Id, WeekdayNumber, Id_Schedule) VALUES (5, 4, 2);
INSERT INTO WorkDays (Id, WeekdayNumber, Id_Schedule) VALUES (6, 6, 2);
INSERT INTO WorkDays (Id, WeekdayNumber, Id_Schedule) VALUES (7, 2, 3);
INSERT INTO WorkDays (Id, WeekdayNumber, Id_Schedule) VALUES (8, 4, 3);
INSERT INTO WorkDays (Id, WeekdayNumber, Id_Schedule) VALUES (9, 6, 3);
INSERT INTO WorkDays (Id, WeekdayNumber, Id_Schedule) VALUES (10, 1, 4);
INSERT INTO WorkDays (Id, WeekdayNumber, Id_Schedule) VALUES (11, 3, 4);
INSERT INTO WorkDays (Id, WeekdayNumber, Id_Schedule) VALUES (12, 5, 4);
INSERT INTO WorkDays (Id, WeekdayNumber, Id_Schedule) VALUES (13, 1, 5);
INSERT INTO WorkDays (Id, WeekdayNumber, Id_Schedule) VALUES (14, 3, 5);
INSERT INTO WorkDays (Id, WeekdayNumber, Id_Schedule) VALUES (15, 5, 5);
SET IDENTITY_INSERT WorkDays OFF
GO

--��������
SELECT * 
FROM WorkDays

SELECT *
FROM Schedules

SELECT Schedules.LastName AS 'Worker', WeekdayNumber AS 'Work Weekday Number'
FROM WorkDays 
	 JOIN Schedules ON Schedules.Id = Id_Schedule
WHERE Schedules.LastName = 'Freeman'