﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LINQToDataSet
{
    public partial class Form : System.Windows.Forms.Form
    {
        private readonly string _connectionString;
        private readonly SqlConnection _connection;
        private string _selectedLastName;
        private readonly DataSet _schedulesDataSet;
        private readonly SqlDataAdapter _adapter1;
        private readonly DataSet _workDaysDataSet;
        private readonly SqlDataAdapter _adapter2;
        private readonly DataTable _schedules;
        private readonly DataTable _workDays;
        public Form()
        {
            InitializeComponent();
            _connectionString = @"Data Source=(local);Initial Catalog=WorkDays;Integrated Security=True";
            _connection = new SqlConnection(_connectionString);

            _schedulesDataSet = new DataSet();
            _adapter1 = new SqlDataAdapter("SELECT * FROM Schedules", _connection);
            _adapter1.Fill(_schedulesDataSet, "Schedules");

            _workDaysDataSet = new DataSet();
            _adapter2 = new SqlDataAdapter("SELECT * FROM WorkDays", _connection);
            _adapter2.Fill(_workDaysDataSet, "WorkDays");

            _schedules = _schedulesDataSet.Tables["Schedules"];
            _workDays = _workDaysDataSet.Tables["WorkDays"];

            dataGrid.RowHeadersVisible = false;
            Load += Form_Load;
            comboBox.SelectedValueChanged += ComboBox_SelectedValueChanged;
        }

        private void ComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            _selectedLastName = (string)comboBox.SelectedItem;
            dataGrid.Rows.Clear();
            dataGrid.Columns.Clear();
            dataGrid.Refresh();       
                      
            var res = _schedules.AsEnumerable().Join(_workDays.AsEnumerable(),
                      s => s.Field<long>("Id"),
                      w => w.Field<long>("Id_Schedule"),
                      (s, w) => new
                      {
                          FirstName = s.Field<string>("FirstName"),
                          LastName = s.Field<string>("LastName"),
                          DateIn = s.Field<DateTime>("DateIn"),
                          DateOut = s.Field<DateTime>("DateOut"),
                          WeekdayNumber = w.Field<int>("WeekdayNumber"),
                      });

            var dates = from w in res
                        where w.LastName == _selectedLastName
                        select new
                        {
                            w.DateIn,
                            w.DateOut,
                            w.WeekdayNumber
                        };

            int row = 0, col = 0;
            foreach (var d in dates)
            {
                DayOfWeek dayOfWeek = (DayOfWeek)d.WeekdayNumber;
                DateTime date = d.DateIn;

                var column = new DataGridViewColumn
                {
                    HeaderText = dayOfWeek.ToString(),
                    ReadOnly = true,
                    Name = dayOfWeek.ToString(),
                    CellTemplate = new DataGridViewTextBoxCell()
                };
                dataGrid.Columns.Add(column);
                dataGrid.AllowUserToAddRows = false;
                dataGrid.Rows.Add();

                for (int i = 0; date != d.DateOut; i++)
                {
                    if (date.DayOfWeek == dayOfWeek)
                    {
                        dataGrid.Rows.Add();
                        dataGrid[col, row].Value = date.ToShortDateString();
                        row++;
                    }

                    date = date.AddDays(1);
                }
                row = 0;
                col++;
            }

            var worker = from w in res
                         where w.LastName == _selectedLastName
                         select w;

            firstNameLabel.Text = worker.First().FirstName;
            lastNameLabel.Text = worker.First().LastName;
            dateInLabel.Text = worker.First().DateIn.ToShortDateString();
            dateOutLabel.Text = worker.First().DateOut.ToShortDateString();

            dataGrid.Focus();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            DataSet dataSet = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter("SELECT DISTINCT LastName FROM Schedules", _connection);
            adapter.Fill(dataSet, "Schedules");
            DataTable schedules = dataSet.Tables["Schedules"];

            var lastnames = from w in schedules.AsEnumerable()
                            select w.Field<string>("LastName");

            comboBox.Text = "Select a lastname";
            foreach (var l in lastnames.Distinct())
            {
                comboBox.Items.Add(l);
            }
        }
    }
}
