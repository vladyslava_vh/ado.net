﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace _03_LINQToObjects
{
    [XmlRoot("Phonebook")]
    [Serializable]
    public class Phonebook
    {
        public List<Contact> Contacts { get; }
        public Phonebook()
        {
            Contacts = new List<Contact>();
        }

        public void AddContact(Contact contact)
        {
            try
            {
                var contacts = from c in Contacts
                               where c.Phone == contact.Phone
                               select c;

                if (contacts.Count() >= 1)
                    throw new InvalidOperationException("Invalid operation: this contact already exists");
                
                Contacts.Add(contact);
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("Invalid operation: contact is empty");
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void RemoveContact(Contact contact)
        {
            try
            {
                Contacts.Remove(contact);
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("Invalid operation: contact is empty");
            }
        }

        public void Clear()
        {
            Contacts.Clear();
        }

        public void Print()
        {
            foreach (Contact contact in Contacts)
            {
                Console.WriteLine(contact.ToString());
            }
        }
    }
}
