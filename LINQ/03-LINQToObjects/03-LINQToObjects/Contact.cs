﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.Threading.Tasks;

namespace _03_LINQToObjects
{
    [Serializable]
    public class Contact
    {
        private string _phone = default;
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone
        {
            get => _phone;
            set
            {
                if (Regex.IsMatch(value, @"((\+38|8)[ ]?)?([(]?\d{3}[)]?[\- ]?)?[\d\-]{6,14}"))
                {
                    _phone = value;
                }
                else
                {
                    throw new InvalidOperationException("Invalid phone number");
                }
            }
        }

        public override string ToString()
        {
            return $"{FirstName}\t{LastName}\t{Phone}";
        }
    }
}
