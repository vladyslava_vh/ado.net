﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace _03_LINQToObjects
{
    class Program
    {
        static void Main()
        {
            Phonebook phonebook = new Phonebook();
            Editor editor = new Editor(ref phonebook, "phonebook.xml");
            editor.Read();

            //phonebook.AddContact(new Contact { FirstName = "Harry", LastName = "Potter", Phone = "0971111111" });
            //Contact contact2 = new Contact { FirstName = "Elon", LastName = "Musk", Phone = "+380965555555" };
            //phonebook.AddContact(contact2);

            //editor.Save();

            //phonebook.AddContact(new Contact { FirstName = "Neo", LastName = "Matrix", Phone = "+380981184211" });
            //editor.Save();

            phonebook.Print();

            //editor.SortByName();
        }
    }
}
