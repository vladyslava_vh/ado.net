﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace _03_LINQToObjects
{
    class Editor
    {
        private Phonebook _phonebook;
        private string _path;
        private readonly XmlSerializer _serializer;

        public Editor(ref Phonebook phonebook, string path)
        {
            _phonebook = phonebook;
            _path = path;
            _serializer = new XmlSerializer(typeof(Phonebook));
        }

        public void Read()
        {
            try
            {
                using (FileStream fs = new FileStream(_path, FileMode.Open))
                {
                    var xmlbook = (Phonebook)_serializer.Deserialize(fs);
                    foreach (var contact in xmlbook.Contacts)
                    {
                        _phonebook.AddContact(contact);
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine($"{ex.FileName} not found");
            }
        }

        public Contact SearchContact(string phone)
        {
            var contacts = from c in _phonebook.Contacts
                           where c.Phone == phone
                           select c;

            return contacts.First();
            
        }

        public void ReplaceContact(string phone, Contact newContact)
        {
            _phonebook.RemoveContact(SearchContact(phone));
            _phonebook.AddContact(newContact);
        }

        public void SortByName()
        {
            var contacts = from c in _phonebook.Contacts
                           orderby c.FirstName
                           select c;

            _phonebook.Clear();
            foreach (var c in contacts)
            {
                _phonebook.AddContact(c);
            }
        }

        public void Save()
        {
            using (FileStream fs = new FileStream(_path, FileMode.OpenOrCreate))
            {
                var xml = new XmlSerializer(typeof(Phonebook));
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");//для удаления из файла xsi и xsd
                xml.Serialize(fs, _phonebook, ns);
            }
        }
    }
}
